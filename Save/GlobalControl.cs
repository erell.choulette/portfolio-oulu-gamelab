﻿#define VERBOSE

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LeavingScene.Save
{
    public class GlobalControl : MonoBehaviour
    {
        public static GlobalControl Instance;
        public GameData savedGame = new GameData();
        void Awake () {
            int sceneCount = SceneManager.sceneCountInBuildSettings;
#if VERBOSE
            Debug.Log("Scene index " + SceneManager.GetActiveScene().buildIndex);   
            Debug.Log("Number of scenes " + sceneCount);
#endif
            if (Instance == null)
            {
                DontDestroyOnLoad(gameObject);
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy (gameObject);
            }
        
            savedGame.savedScenes = new SceneData[sceneCount];
            for (int i = 0; i < sceneCount; i++)
            {
                savedGame.savedScenes[i] = new SceneData();
            }
            LoadData();
        }

        public void SaveData()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream saveFile = File.Create (Application.persistentDataPath + "/savedGame.gd");

            formatter.Serialize(saveFile, savedGame);

            saveFile.Close();
        }

        public void LoadData()
        {
            if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream saveFile = File.Open(Application.persistentDataPath + "/savedGame.gd", FileMode.Open);

                savedGame = (GameData) formatter.Deserialize(saveFile);

                saveFile.Close();
            }
        }
        private void OnDestroy()
        {
            SaveData();
        }
    }
}
