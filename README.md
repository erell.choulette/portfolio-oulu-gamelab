# Portfolio - Quacking Awesome
##### *Erell CHOULETTE*

> Oulu GameLAB - OAMK - Fall 2020

## Presentation
This portfolio is a collection of samples of game development skills and competencies I have learned and practiced as a programmer in the development of the mobile game *Quack the Duck* in the *Quacking Awesome* time.

I joined the team at the middle of the project as a second programmer. Thus, my main goal was at first to get into Unity, the project and the team. 
             
I have mostly been in charge of fixing UI related dysfunction and implementing new features as Unity analytics, death screen, save, tasks and milestones; and I have let the lead developer improve what was already existing, especially the enemies AI, and the tasks requiring a more detailed knowledge about Unity behavior.

Based on the game development roles / skills listed at [Game Badges](https://kumu.io/gamebadges/gamebadges/#gamebadges), I gained some competences of *Data structures*, *Game engines* and *Engine Programming* but mostly of *Coding*.

## Competences

### Coding - *Bronze level* 
*Can program software for games with one programming language.*

-	I have learned a new language (C#) and can program fluently with it,
-	I am able to use normal coding concepts,
-	I learned the basis of object-oriented programming (example: `Tasks`) and I am now able to program in different programming paradigms (procedural, object-oriented and functional),
-	I understand the importance of modularity and encapsulation,
-	I am able to do basic and coding-specific version control such as pull, push, merge and conflict resolution (example: this repository),
-	I am able to work with structural data exchange format (example: `Save`),
-	I can communicate advanced theories and concepts in game programming such as coroutines, threading and event handling, (example: `Death`),

### Data structures - *Bronze level*
*Understands how data structures affect on performance and can choose the right data structure based on context.*

- Utilize basic data structures such as linked lists, arrays, dictionaries/key-value-pairs, (example: `Save`),
- Make choices about mutability and immutability.

### Game engines competences – *Bronze level* 
*Can use at least one game engine*
-	I learned to use all basic functionality of one game engine from the core workflows to publishing a game, (example: `Analytics`),
-	Program in basic level in one language of one game engine (C#),
-	Develop scripts and gameplay programming workflows,
-	Include and configure audio: sound FX and music,
-	Create user interface elements.
![Milestones](./UI/Milestones.JPG)

### Engine Programming – *Bronze level* 
*Can create individual engine features and understands how they work within the engine.*
-	I learned how to apply core concepts of engine programming such as collision detection (example: `Death`),
-	I know how to use most common algorithms in game development.
